﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="chart.aspx.cs" Inherits="FillReportOnline.chart" %>

<%@ Register assembly="FarPoint.Web.Spread, Version=9.40.20161.0, Culture=neutral, PublicKeyToken=327c3516b1b18457" namespace="FarPoint.Web.Spread" tagprefix="FarPoint" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="resource/style/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="resource/script/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">
        $.ready = function () {
            //addcover();
            //document.onreadystatechange = delcover;
        }

        function addcover() {

            var width = $(document).width();
            var height = $(document).height();

            $("body").append("<div id='cover'></div>");
            $("#cover").width(width);
            $("#cover").height(height);
            $("#cover").css("opacity", "0.4");
            $("#cover").css("background-color", "#cccccc");
            $("#cover").css("position", "absolute");
            $("#cover").css("left", "0");
            $("#cover").css("top", "0");
            $("#cover").css("z-index", "10000");
            $("#cover").append("<img id='loading' src='resource/images/ajax-loader .gif' /> ");

            $("#loading").css("padding", (height / 2 - 11) + "px 0 0 " + (width / 2 - 61) + "px");
        }

        function delcover() {
            if (document.readyState == "complete") {
                $("#cover").remove();
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="report">
        <FarPoint:FpSpread ID="FpSpread1" runat="server" BorderColor="Black" BorderStyle="Solid"
            BorderWidth="1px" Height="600px" Width="100%">
            <CommandBar BackColor="Control" ButtonFaceColor="Control" ButtonHighlightColor="ControlLightLight"
                ButtonShadowColor="ControlDark">
            </CommandBar>
            <Sheets>
                <FarPoint:SheetView SheetName="Sheet1">
                </FarPoint:SheetView>
            </Sheets>
        </FarPoint:FpSpread>
        <div class="description" style="padding-right: 30px; padding-left: 50px">
            Spread支持85种丰富多彩的图表效果。基于工作表的数据直接生成图表，操作简单。本例中展示了 Spread .NET 9 中的波形图、</br>区域销售图、产品销售图及销售经理业绩图。
        </div>
    </div>
    </form>
</body>
</html>
