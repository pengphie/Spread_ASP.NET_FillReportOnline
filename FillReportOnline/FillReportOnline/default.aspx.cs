﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FillReportOnline
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                return;
            }
            if (Session["skin"]==null)
            {
                Session["skin"] = 9;
            }
            LoadSkins();
        }

        public void LoadSkins()
        {
            cbSkins.AutoPostBack = true;

            //Loop through and display the default skin names
            foreach (FarPoint.Web.Spread.SheetSkin fpskin in FarPoint.Web.Spread.DefaultSkins.Skins)
                cbSkins.Items.Add(fpskin.Name);
        }

        protected void cbSkins_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["skin"] = this.cbSkins.SelectedIndex;
        }
    }
}