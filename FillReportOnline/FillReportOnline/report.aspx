﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="report.aspx.cs" Inherits="FillReportOnline.report" %>

<%@ Register assembly="FarPoint.Web.Spread, Version=9.40.20161.0, Culture=neutral, PublicKeyToken=327c3516b1b18457" namespace="FarPoint.Web.Spread" tagprefix="FarPoint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="resource/style/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cellClass
        {
            text-decoration: underline;
            color: Blue;
            text-align: center;
            cursor: pointer;
        }
        td
        {
            text-align: center;
        }
    </style>
    <script type="text/javascript" src="resource/script/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">
        $.ready = function () {
            //addcover();
            //document.onreadystatechange = delcover;
        }

        function addcover() {

            var width = $(document).width();
            var height = $(document).height();

            $("body").append("<div id='cover'></div>");
            $("#cover").width(width);
            $("#cover").height(height);
            $("#cover").css("opacity", "0.4");
            $("#cover").css("background-color", "#cccccc");
            $("#cover").css("position", "absolute");
            $("#cover").css("left", "0");
            $("#cover").css("top", "0");
            $("#cover").css("z-index", "10000");
            $("#cover").append("<img id='loading' src='resource/images/ajax-loader .gif' /> ");

            $("#loading").css("padding", (height / 2 - 11) + "px 0 0 " + (width / 2 - 61) + "px");
        }

        function delcover() {
            if (document.readyState == "complete") {
                $("#cover").remove();
            }
        }

    </script>
    <script language="javascript" type="text/javascript">

        function del() {
            var ss = document.getElementById("FpSpread1");
            var activeCol = ss.GetActiveCol();
            var activeRow = ss.GetActiveRow();

            if (activeCol == 9) {

                if (confirm("确定删除该行？")) {
                    ss.CallBack("DeleteRow");
                    ss.Delete();
                    setFocus(ss);
                }
            }
            else if (activeCol == 8&&ss.GetValue(activeRow,activeCol)!="点击保存") {
                alert("本列为指令列。双击前八列编辑。");
            }
            else if (activeCol == 8&&ss.GetValue(activeRow,activeCol)=="点击保存") {
                    var cellid=ss.GetValue(activeRow,0);
                    var celldate=ss.GetValue(activeRow,1);
                    var cellpro=ss.GetValue(activeRow,2);
                    var cellno=ss.GetValue(activeRow,3);
                    var cellincome=ss.GetValue(activeRow,4);
                    var cellregion=ss.GetValue(activeRow,5);
                    var cellsaler=ss.GetValue(activeRow,6);
                    var cellnote=ss.GetValue(activeRow,7);

                    ss.CallBack("btnupdate"+"="+cellid+"="+celldate+"="+cellpro+"="+cellno+"="+cellincome+"="+cellregion+"="+cellsaler+"="+cellnote);
                    setFocus(ss);
            }
        }

        function edit() {
            var ss = document.getElementById("FpSpread1");
            var activeCol = ss.GetActiveCol();
            var activeRow = ss.GetActiveRow();

            if (activeCol != 8 && activeCol !== 9) {

                var cell = ss.GetCellByRowCol(activeRow, 8);
                cell.removeAttribute("FpCellType");
                ss.SetValue(activeRow, 8, "点击保存", true);
                cell.setAttribute("FpCellType", "readonly");
            }
        }

        function btnOver(theTD, ftbName, imageOver, imageDown) {
            event.srcElement.style.backgroundColor = "lightsteelblue";
        }
        function btnOut(theTD, ftbName, imageOver, imageDown) {
            event.srcElement.style.backgroundColor = "";
        }
        function checkformat() {

            var fileupload = this.document.getElementById("<%=FileUpload1.ClientID %>");
            var extend = fileupload.value.substring(fileupload.value.lastIndexOf(".") + 1);

            if (extend == "") {
                alert("请上传 xls 或 xlsx 格式文件!");
                return false;
            }
            else {
                if (!(extend == "xls" || extend == "xlsx")) {
                    alert("请上传 xls 或 xlsx 格式文件!");
                    return false;
                }
            }
            return true;
        }
        function setFocus(ss) {
            if (document.all != null) {
                ss.focus();
            } else {
                //the_fpSpread.SetPageActiveSpread(ss);
                //the_fpSpread.Focus(ss);
                ss.focus();
            }
        }

        function FontBold() {
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("FontBold");
            setFocus(ss);
        }
        function FontItalic() {
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("FontItalic");
            setFocus(ss);
        }
        function SetFontName(name) {
            if (document.all != null) document.body.focus();
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("FontName." + name);
            setFocus(ss);
        }
        function SetFontSize(size) {
            if (document.all != null) document.body.focus();
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("FontSize." + size);
            setFocus(ss);
        }
        function FontUnderline() {
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("FontUnderline");
            setFocus(ss);
        }
        function AlignLeft() {
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("AlignLeft");
            setFocus(ss);
        }
        function AlignCenter() {
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("AlignCenter");
            setFocus(ss);
        }
        function AlignRight() {
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("AlignRight");
            setFocus(ss);
        }
        function SetForeColor(color) {
            if (document.all != null) document.body.focus();
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("ForeColor." + color);
            setFocus(ss);
        }
        function SetBackColor(color) {
            if (document.all != null) document.body.focus();
            var ss = document.getElementById("FpSpread1");
            ss.CallBack("BackColor." + color);
            setFocus(ss);
        }
        function Cut() {
            var ss = document.getElementById("FpSpread1");
            ss.Clear();
        }
        function Copy() {
            var ss = document.getElementById("FpSpread1");
            ss.Copy();
        }
        function Paste() {
            var ss = document.getElementById("FpSpread1");
            ss.Paste();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="ribbon" style="overflow: hidden">
            <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td>
                        <table>
                            <tr style="height: 30px;">
                                <td border="0">
                                    <img alt="" src="resource/images/ajax/toolbar.Horizontal.start.gif" border="0" />
                                </td>
                                <td>
                                    切换报表：
                                </td>
                                <td>
                                    <asp:DropDownList Style="font-family: 微软雅黑" ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"
                                        AutoPostBack="True">
                                        <asp:ListItem>年报表</asp:ListItem>
                                        <asp:ListItem>日报表</asp:ListItem>
                                        <asp:ListItem>销售明细表</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    更换模版：
                                </td>
                                <td>
                                    <asp:FileUpload Style="font-family: 微软雅黑" ID="FileUpload1" runat="server" />
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="Button1" runat="server" Style="font-family: 微软雅黑" Text="确认更换" OnClientClick="return checkformat()"
                                        OnClick="Button1_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td>
                                    <img alt="" src="resource/images/ajax/toolbar.Horizontal.start.gif" border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Bold" onmouseover="btnOver();" onclick="FontBold();"
                                    onmouseout="btnOut();">
                                    <img height="20" alt="Bold" src="resource/images/ajax/bold.gif" width="21" border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Italic" onmouseover="btnOver();" onclick="FontItalic();"
                                    onmouseout="btnOut();">
                                    <img height="20" alt="Italic" src="resource/images/ajax/italic.gif" width="21" border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Underline" onmouseover="btnOver();" onclick="FontUnderline();"
                                    onmouseout="btnOut();">
                                    <img height="20" alt="Underline" src="resource/images/ajax/underline.gif" width="21"
                                        border="0" />
                                </td>
                                <td>
                                    <img alt="" src="resource/images/ajax/separator.Horizontal.gif" border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Justify Left" onmouseover="btnOver();" onclick="AlignLeft();"
                                    onmouseout="btnOut();">
                                    <img height="20" alt="Justify Left" src="resource/images/ajax/justifyleft.gif" width="21"
                                        border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Justify Center" onmouseover="btnOver();" onclick="AlignCenter();"
                                    onmouseout="btnOut();">
                                    <img height="20" alt="Justify Center" src="resource/images/ajax/justifycenter.gif"
                                        width="21" border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Justify Right" onmouseover="btnOver();" onclick="AlignRight();"
                                    onmouseout="btnOut();">
                                    <img height="20" alt="Justify Right" src="resource/images/ajax/justifyright.gif"
                                        width="21" border="0" />
                                </td>
                                <td>
                                    <img alt="" src="resource/images/ajax/separator.Horizontal.gif" border="0" />
                                </td>
                                <td style="padding-left: 4px">
                                    <select style="width: 75px" tabindex="-1" onchange="SetFontName(this[this.selectedIndex].text);this.selectedIndex=0;">
                                        <option value="" selected="selected">Font</option>
                                        <option value="Arial">Arial</option>
                                        <option value="Courier New">Courier New</option>
                                        <option value="Garamond">Garamond</option>
                                        <option value="Georgia">Georgia</option>
                                        <option value="Tahoma">Tahoma</option>
                                        <option value="Times New Roman">Times</option>
                                        <option value="Verdana">Verdana</option>
                                    </select>
                                </td>
                                <td style="padding-left: 4px">
                                    <select tabindex="-1" style="width: 52px" onchange="SetFontSize(this[this.selectedIndex].text);this.selectedIndex=0;">
                                        <option value="" selected="selected">Size</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </select>
                                </td>
                                <td style="padding-left: 4px">
                                    <select style="width: 75px" tabindex="-1" onchange="SetForeColor(this[this.selectedIndex].text);this.selectedIndex=0;">
                                        <option value="" selected="selected">Color</option>
                                        <option style="color: #ffffff; background-color: black" value="#000000">Black</option>
                                        <option style="background-color: gray" value="#808080">Gray</option>
                                        <option style="background-color: #A9A9A9" value="#A9A9A9">DarkGray</option>
                                        <option style="background-color: #D3D3D3" value="#D3D3D3">LightGray</option>
                                        <option style="background-color: white" value="#FFFFFF">White</option>
                                        <option style="background-color: #7FFFD4" value="#7FFFD4">Aquamarine</option>
                                        <option style="background-color: blue" value="#0000FF">Blue</option>
                                        <option style="color: #ffffff; background-color: navy" value="#000080">Navy</option>
                                        <option style="color: #ffffff; background-color: purple" value="#800080">Purple</option>
                                        <option style="background-color: #FF1493" value="#FF1493">DeepPink</option>
                                        <option style="background-color: #EE82EE" value="#EE82EE">Violet</option>
                                        <option style="background-color: #FFC0CB" value="#FFC0CB">Pink</option>
                                        <option style="color: #ffffff; background-color: #006400" value="#006400">DarkGreen</option>
                                        <option style="color: #ffffff; background-color: green" value="#008000">Green</option>
                                        <option style="background-color: #9ACD32" value="#9ACD32">YellowGreen</option>
                                        <option style="background-color: yellow" value="#FFFF00">Yellow</option>
                                        <option style="background-color: #FFA500" value="#FFA500">Orange</option>
                                        <option style="background-color: red" value="#FF0000">Red</option>
                                        <option style="background-color: #A52A2A" value="#A52A2A">Brown</option>
                                        <option style="background-color: #DEB887" value="#DEB887">BurlyWood</option>
                                        <option style="background-color: #F5F5DC" value="#F5F5DC">Beige</option>
                                    </select>
                                </td>
                                <td style="padding-left: 4px">
                                    <select style="width: 85px" tabindex="-1" onchange="SetBackColor(this[this.selectedIndex].text);this.selectedIndex=0;">
                                        <option value="" selected="selected">BackColor</option>
                                        <option style="color: #ffffff; background-color: black" value="#000000">Black</option>
                                        <option style="background-color: gray" value="#808080">Gray</option>
                                        <option style="background-color: #A9A9A9" value="#A9A9A9">DarkGray</option>
                                        <option style="background-color: #D3D3D3" value="#D3D3D3">LightGray</option>
                                        <option style="background-color: white" value="#FFFFFF">White</option>
                                        <option style="background-color: #7FFFD4" value="#7FFFD4">Aquamarine</option>
                                        <option style="background-color: blue" value="#0000FF">Blue</option>
                                        <option style="color: #ffffff; background-color: navy" value="#000080">Navy</option>
                                        <option style="color: #ffffff; background-color: purple" value="#800080">Purple</option>
                                        <option style="background-color: #FF1493" value="#FF1493">DeepPink</option>
                                        <option style="background-color: #EE82EE" value="#EE82EE">Violet</option>
                                        <option style="background-color: #FFC0CB" value="#FFC0CB">Pink</option>
                                        <option style="color: #ffffff; background-color: #006400" value="#006400">DarkGreen</option>
                                        <option style="color: #ffffff; background-color: green" value="#008000">Green</option>
                                        <option style="background-color: #9ACD32" value="#9ACD32">YellowGreen</option>
                                        <option style="background-color: yellow" value="#FFFF00">Yellow</option>
                                        <option style="background-color: #FFA500" value="#FFA500">Orange</option>
                                        <option style="background-color: red" value="#FF0000">Red</option>
                                        <option style="background-color: #A52A2A" value="#A52A2A">Brown</option>
                                        <option style="background-color: #DEB887" value="#DEB887">BurlyWood</option>
                                        <option style="background-color: #F5F5DC" value="#F5F5DC">Beige</option>
                                    </select>
                                </td>
                                <td>
                                    <img alt="" src="resource/images/ajax/separator.Horizontal.gif" border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Cut" onmouseover="btnOver();" onclick="Cut();" onmouseout="btnOut();">
                                    <img height="20" alt="Cut" src="resource/images/ajax/cut.gif" width="21" border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Copy" onmouseover="btnOver();" onclick="Copy();"
                                    onmouseout="btnOut();">
                                    <img height="20" alt="Copy" src="resource/images/ajax/copy.gif" width="21" border="0" />
                                </td>
                                <td class="btnnormal" id="ss_btn_Paste" onmouseover="btnOver();" onclick="Paste();"
                                    onmouseout="btnOut();">
                                    <img height="20" alt="Paste" src="resource/images/ajax/paste.gif" width="21" border="0" />
                                </td>
                                <td class="btnnormal" id="Td1" onmouseover="btnOver();" onclick="Paste();" onmouseout="btnOut();">
                                    <img height="20" alt="Paste" src="resource/images/ajax/paste.gif" width="21" border="0" />
                                </td>
                                <td>
                                    <img alt="" src="resource/images/ajax/separator.Horizontal.gif" border="0" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <img alt="" src="resource/images/ajax/toolbar.Horizontal.end.gif" border="0" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="report">
            <FarPoint:FpSpread ID="FpSpread1" runat="server" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px" Height="532px" Width="100%" OnButtonCommand="FpSpread1_ButtonCommand1"
                OnUpdateCommand="FpSpread1_UpdateCommand">
                <CommandBar BackColor="Control" ButtonFaceColor="Control" ButtonHighlightColor="ControlLightLight"
                    ButtonShadowColor="ControlDark">
                    <Background BackgroundImageUrl="SPREADCLIENTPATH:/img/cbbg.gif"></Background>
                </CommandBar>
                <Sheets>
                    <FarPoint:SheetView SheetName="Sheet1">
                    </FarPoint:SheetView>
                </Sheets>
                <TitleInfo BackColor="#E7EFF7" ForeColor="" HorizontalAlign="Center" VerticalAlign="NotSet"
                    Font-Size="X-Large">
                </TitleInfo>
            </FarPoint:FpSpread>
            <div class="description" style="padding-right: 30px; padding-left: 50px">
                通过 Spread 可以实现复杂商业文档，比如复杂的订单、发票、保单、报税表等。本例中展示了销售明细报表、销售年报表及销售日报表。
            </div>
        </div>
    </div>
    </form>
</body>
</html>
