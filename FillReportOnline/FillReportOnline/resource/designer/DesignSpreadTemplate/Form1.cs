﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace DesignSpreadTemplate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Download("http://www.gcpowertools.com.cn/LiveSamples/Spread/ASPNET/onlinereport/resource/template/report.xls", Application.StartupPath + "\\");

            this.fpSpread1.Open("report.xls");
            this.fpSpreadDesigner1.ShowDialog(this.fpSpread1);
            Application.Exit();
        }

        /// <summary>
        /// 下载服务器文件至客户端
        /// </summary>
        /// <param name="uri">被下载的文件地址</param>
        /// <param name="savePath">另存放的目录</param>
        public static bool Download(string uri, string savePath)
        {
            string fileName;  //被下载的文件名
            if (uri.IndexOf("\\") > -1)
            {
                fileName = uri.Substring(uri.LastIndexOf("\\") + 1);
            }
            else
            {
                fileName = uri.Substring(uri.LastIndexOf("/") + 1);
            }


            if (!savePath.EndsWith("/") && !savePath.EndsWith("\\"))
            {
                savePath = savePath + "/";
            }

            savePath += fileName;   //另存为的绝对路径＋文件名

            WebClient client = new WebClient();
            if (client.Proxy != null)
            {
                client.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
            }

            try
            {
                client.DownloadFile(uri, savePath);
            }
            catch
            {
                return false;
            }
            return true;
        }

    }
}
